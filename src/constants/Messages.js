export const MSG_ADD_TO_CART_SUCCESS = 'Đã thêm vào giỏ hàng !';
export const MGS_UPDATE_CART_SUCCESS = 'Cập nhật giỏ hàng thành công !';
export const MSG_DELETE_PRODUCT_IN_CART_SUCCESS = 'Đã xóa sản phẩm khỏi giỏ hàng !';
export const MSG_Fail = 'Hành động thất bại !';
export const MGS_ADD_MAX_INVENTORY_PRODUCT = 'Sản phẩm đã đạt mức tối đa !';
export const MSG_CART_EMPTY = 'Chưa có sản phẩm nào trong giỏ hàng !';
export const MSG_WELLCOME = 'Chào mừng bạn đến với Mua sắm online của Cart-Page !';