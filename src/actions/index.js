import * as types from './../constants/ActionTypes';

export const addToCart = (product, quantity) =>{
    return {
        type : types.ADD_TO_CART,
        product,
        quantity
    }
}

export const deleteProductInCart = (product) => {
    return {
        type : types.DELETE_PRODUCT_IN_CART ,
        product
    }
}

export const updateQuantityProductInCart = (product, quantity) => {
    return {
        type : types.UPDATE_QUANTITY_PRODUCT_IN_CART ,
        product,
        quantity
    }
}

export const changeMessages = (message) =>{
    return {
        type : types.CHANGE_MESSAGES,
        message
    }
}