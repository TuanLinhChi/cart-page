import React, {Component} from 'react';
import Products from './../components/Products';
import { connect } from 'react-redux';
import Product from './../components/Products/Product';
import PropTypes from 'prop-types';
import * as actions from './../actions/index';
import * as Messages from './../constants/Messages';

class ProductsContainer extends Component {
	//return all element product
	showProducts = (products) =>{
		var result = null;
		if(products.length > 0){
			result = products.map((product, index)=>{
				return <Product 
					key={index} 
					product={product}
					onAddToCart={this.addCart}
				/>;
			});
		}
		return result;
	}

	addCart = (product) => {
		this.props.onAddToCart(product);
		this.props.onChangeMessage(Messages.MSG_ADD_TO_CART_SUCCESS);
	}

	render() {
        var {products} = this.props; 
		return (
			<Products>
				{ this.showProducts(products) }
			</Products>
		);
	}
}

//use propTypes to check validators type
ProductsContainer.propTypes = {
	products : PropTypes.arrayOf(
		//shape for object
		PropTypes.shape({
			id : PropTypes.number.isRequired,
			name : PropTypes.string.isRequired,
			image : PropTypes.string.isRequired,
			description : PropTypes.string.isRequired,
			price : PropTypes.number.isRequired,
			inventory : PropTypes.number.isRequired,
			rating : PropTypes.number.isRequired
		})
	).isRequired,
	onAddToCart: PropTypes.func.isRequired,
	onChangeMessage: PropTypes.func.isRequired,
}

const mapStateToProps = (state, ownProps) => {
	return {
		products : state.products
	}
}

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		onAddToCart: (product) => {
			dispatch(actions.addToCart(product, 1));
		},
		onChangeMessage: (message) => {
			dispatch(actions.changeMessages(message));
		}
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductsContainer);