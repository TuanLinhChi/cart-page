import React, {Component} from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Messages from './../components/Messages';

class MessageContainer extends Component {
	render() {
		var {messages} = this.props;
		return (
			<Messages messages={messages}></Messages>
		);
	}
}

//use propTypes to check validators type
MessageContainer.propTypes = {
	messages : PropTypes.string.isRequired
}

const mapStateToProps = (state, ownProps) => {
	return {
		messages: state.message
	}
}

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageContainer);