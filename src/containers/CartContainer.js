import React, {Component} from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Cart from './../components/Cart';
import CartItem from './../components/Cart/CartItem';
import CartResult from './../components/Cart/CartResult';
import * as actions from './../actions/index';
import * as Messages from './../constants/Messages';

class CartContainer extends Component {
	//return all element cart item
	showCartItem = (cart) => {
		var result = <tr><td>{Messages.MSG_CART_EMPTY}</td></tr>;
		if(cart.length > 0){
			result = cart.map((cartItem, index)=>{
				return <CartItem 
					key={ index } 
					cartItem={ cartItem } 
					total={ this.getTotalCartItem(cartItem.product.price, cartItem.quantity) }
					onDeleteProductInCart={ this.onDeleteProductInCart }
					onUpQuantityProduct={ this.onUpdateQuantityProduct }
					onDownQuantityProduct={ this.onUpdateQuantityProduct }
				/>;
			});
		}
		return result;
	}

	onUpdateQuantityProduct = (product, quantity) => {
		if(quantity > 0 && quantity <= product.inventory){
			this.props.onUpdateQuantityProduct(product, quantity);
			this.props.onChangeMessages(Messages.MGS_UPDATE_CART_SUCCESS);
		}else if(quantity <= 0){
			this.props.onChangeMessages(Messages.MSG_Fail);
		}else{
			this.props.onChangeMessages(Messages.MGS_ADD_MAX_INVENTORY_PRODUCT);
		}
	}

	onDeleteProductInCart = (cart) => {
		this.props.onDeleteProductInCart(cart);
		this.props.onChangeMessages(Messages.MSG_DELETE_PRODUCT_IN_CART_SUCCESS);
	}

	showCartResult = (cart) => {
		if(cart.length > 0){
			return <CartResult total={ this.getTotal(cart) }/>;
		}
	}

	getTotal = (cart) => {
		let total = 0;
		if(cart.length > 0){
			cart.forEach(function(element){
				total += element.product.price*element.quantity;
			});
		}
		return total;
	}

	getTotalCartItem = (cost, quantity) => {
		return cost*quantity;
	}

	render() {
		var {cart} = this.props;
		return (
			<Cart>
				{ this.showCartItem(cart) }
				{ this.showCartResult(cart) }
			</Cart>
		);
	}
}

//use propTypes to check validators type
CartContainer.propTypes = {
	cart : PropTypes.arrayOf(
		PropTypes.shape({
			product: PropTypes.shape({
				id : PropTypes.number.isRequired,
				name : PropTypes.string.isRequired,
				image : PropTypes.string.isRequired,
				description : PropTypes.string.isRequired,
				price : PropTypes.number.isRequired,
				inventory : PropTypes.number.isRequired,
				rating : PropTypes.number.isRequired
			}).isRequired,
			quantity: PropTypes.number.isRequired
		})
	).isRequired,
	onDeleteProductInCart: PropTypes.func.isRequired,
	onChangeMessages: PropTypes.func.isRequired,
	onUpdateQuantityProduct: PropTypes.func.isRequired,
}

const mapStateToProps = (state, ownProps) => {
	return {
		cart : state.cart
	}
}

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		onDeleteProductInCart: (cart)=>{
			dispatch(actions.deleteProductInCart(cart));
		},
		onChangeMessages: (messages) => {
			dispatch(actions.changeMessages(messages));
		},
		onUpdateQuantityProduct: (cart, quantity) =>{
			dispatch(actions.updateQuantityProductInCart(cart, quantity));
		}
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(CartContainer);