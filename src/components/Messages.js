import React, {Component} from 'react';

class Messages extends Component {
	render() {
		var {messages} = this.props;
		return (
			<h3>
				<span className="badge amber darken-2">{messages}</span>
			</h3>
		);
	}
}

export default Messages;