import React, {Component} from 'react';

class Product extends Component {
	//return all element star
	showRating = (rating) => {
		var result = [];
		if(rating > 0 && rating <= 5){
			for(let i=0; i<rating; i++){
				result.push(<i key={i} className="fa fa-star"></i>);
			}
			for(let i=5; i>rating; i--){
				result.push(<i key={i} className="fa fa-star-o"></i>);
			}
		}else if(rating > 5){
			for(let i=0; i<5; i++){
				result.push(<i key={i} className="fa fa-star"></i>);
			}
		}
		return result;
	}

	render() {
		var {product} = this.props;
		return (
			<div className="col-lg-4 col-md-6 mb-r">
				<div className="card text-center card-cascade narrower">
					<div className="view overlay hm-white-slight z-depth-1">
						<img src={ product.image }
							className="img-fluid" alt={ product.name } />
						<button>
							<div className="mask waves-light waves-effect waves-light"></div>
						</button>
					</div>
					<div className="card-body">
						<h4 className="card-title">
							<strong>
								<span>{ product.name }</span>
							</strong>
						</h4>
						<ul className="rating">
							<li>
								{ this.showRating(product.rating) }
							</li>
						</ul>
						<p className="card-text">{ product.description }</p>
						<div className="card-footer">
							<span className="left">{ product.price } VND</span>
							<span className="right">
								<button
									onClick={ ()=>{this.props.onAddToCart(product)} }
									className="btn-floating blue-gradient" 
									data-toggle="tooltip" data-placement="top" 
									data-original-title="Add to Cart">
									<i className="fa fa-shopping-cart"></i>
								</button>
							</span>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Product;