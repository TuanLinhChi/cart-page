import React, {Component} from 'react';

class CartItem extends Component {
	render() {
		var { cartItem, total } = this.props;
		return (
			<tr>
				<th scope="row">
					<img src={ cartItem.product.image } alt={ cartItem.product.name } className="img-fluid z-depth-0" />
				</th>
				<td>
					<h5>
						<strong>{ cartItem.product.name }</strong>
					</h5>
				</td>
				<td>{ cartItem.product.price } VND</td>
				<td className="center-on-small-only">
					<span className="qty">{ cartItem.quantity } </span>
					<div className="btn-group radio-group" data-toggle="buttons">
						<label 
							className="btn btn-sm btn-primary btn-rounded waves-effect waves-light"
							onClick={ ()=>{this.props.onDownQuantityProduct(cartItem.product, cartItem.quantity-1)} }
						>
							<span>—</span>
						</label>
						<label 
							className="btn btn-sm btn-primary btn-rounded waves-effect waves-light"
							onClick={ ()=>{this.props.onUpQuantityProduct(cartItem.product, cartItem.quantity+1)} }
						>
							<span>+</span>
						</label>
					</div>
				</td>
				<td>{ total } VND</td>
				<td>
					<button 
						type="button" 
						className="btn btn-sm btn-primary waves-effect waves-light" 
						data-toggle="tooltip" 
						data-placement="top"
						data-original-title="Remove item"
						onClick={ ()=>{this.onDeleteProductInCart(cartItem.product)} }
					>
						X
					</button>
				</td>
			</tr>
		);
	}

	onDeleteProductInCart = (product) => {
		var check = window.confirm('Bạn có chắc chắn về hành động này?');
		if(check){
			this.props.onDeleteProductInCart(product);
		}
	}
}

export default CartItem;