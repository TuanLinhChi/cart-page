import * as types from './../constants/ActionTypes';
import * as messages from './../constants/Messages';

var initialState = messages.MSG_WELLCOME;

var myReducer = (state = initialState, action) => {
    switch (action.type){
        case types.CHANGE_MESSAGES:
            return action.message;
        default:
            return state;
    }
}

export default myReducer;