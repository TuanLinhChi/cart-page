import * as types from './../constants/ActionTypes';

var data = JSON.parse(localStorage.getItem('carts'));
var initialState = data ? data : [];

var findIndex=(carts, id)=>{
    let result = -1;
    carts.forEach((cart, key)=>{
        if(cart.product.id === id) result = key;
    });
    return result;
}

var myReducer = (state = initialState, action) => {
    var index = -1;
    switch (action.type){
        case types.ADD_TO_CART:
            index = findIndex(state, action.product.id);
            if(index === -1){
                state.push({
                    product: action.product,
                    quantity: action.quantity
                });
            }else{
                state[index].quantity += action.quantity;
            }
            localStorage.setItem('carts', JSON.stringify(state));
            return [...state];
        case types.DELETE_PRODUCT_IN_CART:
            index = findIndex(state, action.product.id);
            if(index !== -1){
                state.splice(index, 1);
            }
            localStorage.setItem('carts', JSON.stringify(state));
            return [...state];
        case types.UPDATE_QUANTITY_PRODUCT_IN_CART:
            index = findIndex(state, action.product.id);
            if(index !== -1 && action.quantity > 0 && action.quantity <= action.product.inventory){
                state[index].quantity = action.quantity;
            }
            localStorage.setItem('carts', JSON.stringify(state));
            return [...state];
        default:
            return [...state];
    }
}

export default myReducer;