//import * as types from './../constants/ActionTypes';

// var findIndex=(tasks, id)=>{
//     let result = -1;
//     tasks.forEach((task, key)=>{
//         if(task.id === id) result = key;
//     });
//     return result;
// }

var initialState = [
    {
        id : 1,
        name : 'Iphone 7 plus',
        image : 'https://store.storeimages.cdn-apple.com/4974/as-images.apple.com/is/image/AppleInc/aos/published/images/H/H0/HH0H2/HH0H2?wid=445&hei=445&fmt=jpeg&qlt=95&op_sharpen=0&resMode=bicub&op_usm=0.5,0.5,0,0&iccEmbed=0&layer=comp&.v=K7ik72',
        description : 'Sản phẩm do apply sản xuất',
        price : 15000000,
        inventory : 10,
        rating : 1
    },
    {
        id : 2,
        name : 'Iphone 8 plus',
        image : 'https://store.storeimages.cdn-apple.com/4974/as-images.apple.com/is/image/AppleInc/aos/published/images/H/H0/HH0H2/HH0H2?wid=445&hei=445&fmt=jpeg&qlt=95&op_sharpen=0&resMode=bicub&op_usm=0.5,0.5,0,0&iccEmbed=0&layer=comp&.v=K7ik72',
        description : 'Sản phẩm do apply sản xuất',
        price : 15000000,
        inventory : 9,
        rating : 2
    },
    {
        id : 3,
        name : 'Iphone 9 plus',
        image : 'https://store.storeimages.cdn-apple.com/4974/as-images.apple.com/is/image/AppleInc/aos/published/images/H/H0/HH0H2/HH0H2?wid=445&hei=445&fmt=jpeg&qlt=95&op_sharpen=0&resMode=bicub&op_usm=0.5,0.5,0,0&iccEmbed=0&layer=comp&.v=K7ik72',
        description : 'Sản phẩm do apply sản xuất',
        price : 15000000,
        inventory : 8,
        rating : 3
    },
    {
        id : 4,
        name : 'Iphone X plus',
        image : 'https://store.storeimages.cdn-apple.com/4974/as-images.apple.com/is/image/AppleInc/aos/published/images/H/H0/HH0H2/HH0H2?wid=445&hei=445&fmt=jpeg&qlt=95&op_sharpen=0&resMode=bicub&op_usm=0.5,0.5,0,0&iccEmbed=0&layer=comp&.v=K7ik72',
        description : 'Sản phẩm do apply sản xuất',
        price : 15000000,
        inventory : 2,
        rating : 5
    },
];

var myReducer = (state = initialState, action) => {
    switch (action.type){
        default:
            return [...state];
    }
}

export default myReducer;