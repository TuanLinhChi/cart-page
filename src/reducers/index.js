import { combineReducers } from 'redux';
import products from './products';
import cart from './cart';
import message from './message';

//combine cac reduce
//khai bao cac reduce de combine
const myReducer = combineReducers({
    products,
    cart,
    message
});

export default myReducer;